package eu.hashconnect.security;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

class RSAVerifierTest {

    @Test
    void encryptAndDecrypt() throws IOException {
        String privateKey = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("private_key.pem"));
        String publicKey = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("public_key.pem"));

        String input = "some string";

        assertTrue(
                RSAVerifier.isSignatureValid(
                        input, RSAVerifier.sign(input, privateKey), publicKey
                )
        );
    }
}
