package eu.hashconnect.security;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class RSAVerifier  {

    private static final String PUBLIC_KEY_TEMPLATE = "(-+BEGIN PUBLIC KEY-+\\r?\\n?|-+END PUBLIC KEY-+\\r?\\n?)";
    private static final String PRIVATE_KEY_TEMPLATE = "(-+BEGIN RSA PRIVATE KEY-+\\r?\\n?|-+END RSA PRIVATE KEY-+\\r?\\n?)";
    private static final String SIGNATURE_ALGORITHM_NAME = "SHA256withRSA";
    private static final String KEY_ALGORITHM_NAME = "RSA";
    private static final String SECURITY_PROVIDER_NAME = "BC";

    public static boolean isSignatureValid(String message, String signature, String key) {
        try {
            return verifySignature(
                    message.getBytes(),
                    loadPemPublicKey(key),
                    Base64.getDecoder().decode(signature)
            );
        } catch (Exception e) {
            return false;
        }
    }

    public static String sign(String message, String privateKeyString) {
        try {
            PrivateKey privateKey = loadPemPrivateKey(privateKeyString);
            byte[] messageToSign = message.getBytes();

            Signature signer = Signature.getInstance(SIGNATURE_ALGORITHM_NAME);
            signer.initSign(privateKey);
            signer.update(messageToSign);
            byte[] binarySignature = signer.sign();

            return Base64.getEncoder().encodeToString(binarySignature);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static boolean verifySignature(byte[] data, PublicKey key, byte[] sig) throws Exception {
        Signature signer = Signature.getInstance(SIGNATURE_ALGORITHM_NAME);
        signer.initVerify(key);
        signer.update(data);

        return signer.verify(sig);
    }

    private static PublicKey loadPemPublicKey(String publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String publicKeyContent = publicKey
                .replaceAll(PUBLIC_KEY_TEMPLATE, "")
                .replace("\n","");

        byte[] keyBytes = Base64.getDecoder().decode(publicKeyContent);

        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_NAME);

        return keyFactory.generatePublic(publicKeySpec);
    }

    private static PrivateKey loadPemPrivateKey(String privateKey)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchProviderException {
        String privateKeyContent = privateKey
                .replaceAll(PRIVATE_KEY_TEMPLATE, "")
                .replace("\n", "");

        byte[] keyBytes = Base64.getDecoder().decode(privateKeyContent);

        Security.addProvider(new BouncyCastleProvider());
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM_NAME, SECURITY_PROVIDER_NAME);

        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(keyBytes);

        return keyFactory.generatePrivate(privateKeySpec);
    }
}
